# scanner_app

### NETWORK SCANNING UTILITIES

This project was started while working on my security+ certification. I've used [Hacker Target](https://hackertarget.com/) for a lot of inspiration.
This project is strictly for learning purposes. 

#### App Structure

This is a pretty simple flask app that executes bash scripts that saves the results to /scans/< tool used >

After the file is saved the script checks the folder and sorts them so the newest item is on top which it then reads to render or email.

Most of the app logic is contained in /app.py

The html is contained in the /templates dir

#### Installation

    git clone https://gitlab.com/joshsisto/scanner_app.git && ./scanner_app/post_install_script

create credentials.py file and update API credentials. Mine are currently set for mailjet

##### Example credentials.py

mailjet_user = 'API USER FOR MAILJET'

mailjet_pass = 'API PASS FOR MAILJET'

If you are using a different SMTP relay provider change it in the send_email.py script
