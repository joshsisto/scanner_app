from flask import Flask, render_template, request, flash, session, redirect, url_for
import os
import glob
from send_email import send_email
from celery import Celery


REPO_DIR = os.path.abspath(os.path.join(
    os.path.dirname(os.path.abspath(__file__))))


app = Flask(__name__)
app.config.from_object("config")
app.secret_key = app.config['SECRET_KEY']
# app.config.from_object("config")


# Set up celery client
client = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
client.conf.update(app.config)


@client.task
def nikto_scan(site, scanner_email):
    scan_site_url = os.system(
        f'sh {REPO_DIR}/bash_scripts/nikto_address.sh {site}')
    prev_scans_dir = (f'{REPO_DIR}/scans/nikto/{site}/*.txt')
    prev_scans_list = glob.glob(prev_scans_dir)
    prev_scans_list.sort()
    prev_scans_list.reverse()
    with open(prev_scans_list[0]) as f:
        prev_scans = f.readlines()
    send_email(scanner_email, f'scan of {site}', str(prev_scans))
    return scan_site_url


@client.task
def sslyze_scan(site, scanner_email):
    scan_site_url = os.system(
        f'sh {REPO_DIR}/bash_scripts/sslyze_address.sh {site}')
    prev_scans_dir = (f'{REPO_DIR}/scans/sslyze/{site}/*.txt')
    prev_scans_list = glob.glob(prev_scans_dir)
    prev_scans_list.sort()
    prev_scans_list.reverse()
    with open(prev_scans_list[0]) as f:
        prev_scans = f.readlines()
    send_email(scanner_email, f'scan of {site}', str(prev_scans))
    return scan_site_url


def scan_site(site):
    scan_site_url = os.system(
        f'sh {REPO_DIR}/bash_scripts/scan_address.sh {site}')
    return scan_site_url


def scan_site_verbose(site):
    scan_site_url = os.system(
        f'sh {REPO_DIR}/bash_scripts/scan_address_verbose.sh {site}')
    return scan_site_url


def wpscan_scan(site):
    scan_site_url = os.system(
        f'sh {REPO_DIR}/bash_scripts/wpscan_address.sh {site}')
    return scan_site_url


def whatweb_scan(site):
    scan_site_url = os.system(
        f'sh {REPO_DIR}/bash_scripts/whatweb_address.sh {site}')
    return scan_site_url


@app.route('/')
def main_page():
    return render_template('main_page.html')


@app.route('/nikto', methods=['POST', 'GET'])
def nikto_scan_post():
    if request.method == 'GET':
        check_credentials = False
        if os.path.isfile('credentials.py') == True:
            check_credentials = True
        return render_template('nikto_scan.html', check_credentials=check_credentials)
    elif request.method == 'POST':
        URL_TO_SCAN = request.form['URL_INPUT']
        email_target = request.form['email']
        task = nikto_scan.delay(URL_TO_SCAN, email_target)
        flash(f"Email will be sent to {email_target} in {URL_TO_SCAN}")
        return render_template('nikto_scan_comp.html', email_dest=email_target, scanned_url=URL_TO_SCAN)


@app.route('/sslyze', methods=['POST', 'GET'])
def sslyze_post():
    if request.method == 'GET':
        check_credentials = False
        if os.path.isfile('credentials.py') == True:
            check_credentials = True
        return render_template('sslyze.html')
    elif request.method == 'POST':
        URL_TO_SCAN = request.form['URL_INPUT']
        email_target = request.form['email']
        task = sslyze_scan.delay(URL_TO_SCAN, email_target)
        flash(f"Email will be sent to {email_target} in {URL_TO_SCAN}")
        return render_template('sslyze.html', check_credentials=check_credentials)


@app.route('/nmap', methods=['POST', 'GET'])
def web_scan_post():
    if request.method == 'POST':
        URL_TO_SCAN = request.form['URL_INPUT']
        verbose_check = request.form.get('verbose')
        if verbose_check == 'verbose':
            print(f'verboseCheck if {verbose_check}')
            site_scan_results = scan_site_verbose(URL_TO_SCAN)
        else:
            site_scan_results = scan_site(URL_TO_SCAN)
            print(f'verboseCheck else {verbose_check}')
        prev_scans_dir = (f'{REPO_DIR}/scans/nmap/{URL_TO_SCAN}/*.nmap')
        prev_scans_list = glob.glob(prev_scans_dir)
        prev_scans_list.sort()
        prev_scans_list.reverse()
        with open(prev_scans_list[0]) as f:
            prev_scans = f.readlines()
        return render_template('nmap_scan.html', results=prev_scans)
    else:
        return render_template('nmap_scan.html')


@app.route('/wpscan', methods=['POST', 'GET'])
def wpscan_post():
    if request.method == 'POST':
        URL_TO_SCAN = request.form['URL_INPUT']
        site_scan_results = wpscan_scan(URL_TO_SCAN)
        prev_scans_dir = (f'{REPO_DIR}/scans/wpscans/*.txt')
        prev_scans_list = glob.glob(prev_scans_dir)
        prev_scans_list.sort()
        # prev_scans_list.reverse()
        with open(prev_scans_list[0]) as f:
            prev_scans = f.readlines()
        return render_template('wpscan.html', results=prev_scans)
    else:
        return render_template('wpscan.html')


@app.route('/whatweb', methods=['POST', 'GET'])
def whatweb_post():
    if request.method == 'POST':
        URL_TO_SCAN = request.form['URL_INPUT']
        site_scan_results = whatweb_scan(URL_TO_SCAN)
        prev_scans_dir = (f'{REPO_DIR}/scans/whatweb/{URL_TO_SCAN}/*.txt')
        prev_scans_list = glob.glob(prev_scans_dir)
        prev_scans_list.sort()
        prev_scans_list.reverse()
        with open(prev_scans_list[0]) as f:
            prev_scans = f.readlines()
        return render_template('whatweb.html', results=prev_scans)
    else:
        return render_template('whatweb.html')


while __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
