#!/bin/bash
# Local Scan

# Absolute path to this script
SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

mkdir $SCRIPTPATH/scans/nmap

cd $SCRIPTPATH

cd ../scans/nmap

VAR_TIME=$(date +%F_%H:%M:%S:%N | sed 's/\(:[0-9][0-9]\)[0-9]*$/\1/')

DEST_HOST=$1

mkdir $DEST_HOST

cd $DEST_HOST

nmap $DEST_HOST -oA $VAR_TIME
