import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os
import glob

try:
    from credentials import mailjet_user, mailjet_pass
except:
    print('credentials file not set correctly')
    smtp_user = ''
    smtp_pass = ''


def send_email(toaddr, subject, message_body):
    fromaddr = "scanner_app@sisto.xyz"  # from email address
    # toaddr = "joshsisto@gmail.com"  # destination email address
    smtp_user = mailjet_user
    smtp_pass = mailjet_pass
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject  # subject

    body = message_body  # body
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('in-v3.mailjet.com', 587)  # e.g. ('in-v3.mailjet.com', 587)
    server.starttls()
    server.login(smtp_user, smtp_pass)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

# send_email("joshsisto@gmail.com", "executed from script", "We waited 2 min")

